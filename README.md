# Nord i3 + i3blocks

![polybar is bloat](https://i.redd.it/2v806q5kqbl91.png)

## Usage

Clone the repo, then run:

```
stow -d /cloned/repo/dir -t ~ nord-i3
```

Don't mind a lot of mess in some of the files. Also picom configuration is
taken from somewhere I can't recall, as well as CPU and memory scripts (tweaked
i3blocks-contrib), most probably also dunstrc. If you'd want to use it, be sure
to check autostart section and keybindings in i3 config, as they might be not 
what you want or need.

*Tip*: Install all themes globally, that may save you a headache.

## Info / dependencies

* WM: i3-gaps
* Bar: i3bar + i3blocks (sysstat and pulsemixer are dependencies, also rofi and [rofi-power-menu](https://github.com/jluttine/rofi-power-menu))
* Compositor: Picom
* Terminal: Alacritty
* Font: [MesloLGS NF](https://github.com/romkatv/powerlevel10k-media/blob/master/MesloLGS%20NF%20Regular.ttf)
* [Wallpaper](https://unsplash.com/photos/vnywo8u6d9c)
* GTK Theme: [Nordic Darker](https://www.gnome-look.org/p/1267246/)
* [Kvantum Theme](https://github.com/EliverLara/Nordic/tree/master/kde/kvantum/Nordic-Darker)
* Icons: [Nordic Darker](https://www.gnome-look.org/p/1733012) (Papirus is a depedency)
* [Cursors](https://www.gnome-look.org/p/1662218)
* Other things for all stuff in dotfiles to work: pactl, playerctl, xss-lock, i3lock

I hope I haven't forgotten anything.
